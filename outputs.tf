output "external_load_balancer_ip" {
  value = google_compute_address.external_load_balancer_ip.address
}