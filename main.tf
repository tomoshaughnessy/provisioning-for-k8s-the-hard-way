provider "google" {
  project = "k8s-the-hard-way-2-344610"
  region  = "us-east1"
  zone    = "us-east1-b"
}

locals {
  cluster_cidr    = "10.200.0.0/16"
  instance_scopes = ["compute-rw", "storage-ro", "service-management", "service-control", "logging-write", "monitoring"]
}

resource "google_compute_network" "vpc_network" {
  name                    = "vpc-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "k8s_subnetwork" {
  name          = "k8s-subnetwork"
  ip_cidr_range = "10.240.0.0/24"
  network       = google_compute_network.vpc_network.name
  region        = "us-east1"
}

resource "google_compute_firewall" "internal_fw" {
  name    = "internal-fw"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  source_ranges = [google_compute_subnetwork.k8s_subnetwork.ip_cidr_range, local.cluster_cidr]
}

resource "google_compute_firewall" "external_fw" {
  name    = "external-fw"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "6443"]
  }
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_address" "external_load_balancer_ip" {
  name = "external-load-balancer-ip"
}

resource "google_compute_instance" "controllers" {
  count          = 3
  name           = "controller-${count.index}"
  tags           = ["k8s-the-hard-way", "controller"]
  can_ip_forward = true
  machine_type   = "e2-standard-2"
  network_interface {

    network_ip = "10.240.0.1${count.index}"
    subnetwork = google_compute_subnetwork.k8s_subnetwork.name
  }
  service_account {

    scopes = local.instance_scopes
  }
  boot_disk {
    initialize_params {
      # GB
      size = 200
      # good support for containerd runtime
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
}

resource "google_compute_instance" "workers" {
  count          = 3
  name           = "worker-${count.index}"
  tags           = ["k8s-the-hard-way", "worker"]
  can_ip_forward = true
  machine_type   = "e2-standard-2"
  network_interface {

    network_ip = "10.240.0.2${count.index}"
    subnetwork = google_compute_subnetwork.k8s_subnetwork.name
  }
  service_account {
    scopes = local.instance_scopes
  }
  metadata = {
    # part of the cluster cidr range in the firewall
    # exposes pod subnet allocations to instances at runtime
    pod-cidr = "10.200.${count.index}.0/24"
  }

  boot_disk {
    initialize_params {
      # GB
      size = 200
      # good support for containerd runtime
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
}